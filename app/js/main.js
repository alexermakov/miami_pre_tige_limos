let prevSlideType;
$(function () {

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'outside',
    });

    $(window).on("load", function () {
        $('body').addClass('loaded')
    })


    $('.js_our_feet__block__x__slider').slick({
        dots: true,
        arrows: true,
        responsive: [{
                breakpoint: 899,
                settings: {
                    variableWidth: true
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    variableWidth: false

                }
            },
        ]
    })


    $(".js_select").each(function (index, element) {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
        })
    });


    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js_field_row--reserve_y_col .js_field_row--reserve_y_field input').change(function(){
        let parent = $(this).closest('.js_field_row--reserve_y_col');

        parent.find('.field_row--reserve_y__radio_content_item.active').slideUp(400,function(){
            $(this).removeClass('active')
        })

        console.log($(this).val())

        parent.find('.field_row--reserve_y__radio_content_item').eq($(this).val()).slideDown(400,function(){
            $(this).addClass('active')
        })

    })



    $('.js_btn_field_reserve--back').click(function (e) {
        e.preventDefault()
        let thisEl = $('.js_reserve__step_x.active')

        thisEl.slideUp(400, function () {
            $(this).removeClass('active')
        });

        thisEl.prev().slideDown(400, function () {
            $(this).addClass('active')
            $('.js_content_reserve__step_list .content_reserve__step_item.active').last().removeClass('active')
        });
    })


    function validateX(thisEl) {
        let goNext = true;
        thisEl.find('input[required],textarea[required]').removeClass('warning')
        thisEl.find('input[required],textarea[required]').each(function () {
            if ($(this).val().length <= 0) {
                goNext = false;
                $(this).addClass('warning')
                console.log('text');
            }

            if ($(this).attr('type') == 'checkbox') {
                if (!$(this).prop('checked')) {
                    goNext = false;
                    $(this).addClass('warning')
                    console.log('radio');
                }
            }

            if ($(this).attr('type') == 'radio') {
                if (!$(this).prop('checked')) {
                    goNext = false;
                    $(this).addClass('warning')
                    console.log('radio');
                }
            }

        })

        console.log(goNext);

        return goNext;
    }


    $('.js_btn_field_reserve--next').click(function (e) {
        e.preventDefault()
        let thisEl = $('.js_reserve__step_x.active');
        if (validateX(thisEl)) {
            thisEl.slideUp(400, function () {
                $(this).removeClass('active')
            });
            thisEl.next().slideDown(400, function () {
                $(this).addClass('active')
                $('.js_content_reserve__step_list .content_reserve__step_item.active').last().next().addClass('active')
            });
        }
    })



    $('.js_btn_field_send').click(function () {
        if (validateX($('.js_reserve__step_x.active'))) {
            $('.js_reserve__form').trigger('submit')
        }
    })

    $('.js_reserve__form').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serialize();
        console.log(formData);
        // send form data

        $('.js_content_reserve__step_list').slideUp(400);

        $('.js_reserve__step_x.active').slideUp(400, function () {
            $(this).removeClass('active')
        });

        $('.js_reserve__step_x--thank').slideDown(400, function () {
            $(this).addClass('active')
            $('.js_reserve__step_x--thank').addClass('active')
        });
    });




    $('.js_faq_item .faq_item__top').click(function () {
        $(this).closest('.js_faq_item').find('.faq_item__content').stop().slideToggle(400, function () {
            $(this).closest('.js_faq_item').toggleClass('active')
        });
    })


    $('.home_type__item__slider .js_home_type__item__slider').on('destroy', function (event, slick) {
        if (!$('.home_type__item__slider.active .js_home_type__item__slider').hasClass('slick-initialized')){
            let slider = $('.home_type__item__slider.active .js_home_type__item__slider');


            slider.slick({
                dots:false,
                arrows:true
            })


        }
    })


    $('.home_type__item__slider.active .js_home_type__item__slider').slick({
        dots: false,
        arrows: true
    })


    $('.js_home_page__type__tab a').click(function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_home_page__type__tab a.active').removeClass('active')
            $(this).addClass('active')

            prevSlideType = $('.js_home_page__type_content .home_type__item__slider.active');
            $('.js_home_page__type_content .home_type__item__slider.active').slideUp(400, function () {
                $(this).removeClass('active')
            });
            $($('.js_home_page__type_content .home_type__item__slider')[$(this).index()]).slideDown(400, function () {

                that = $(this);
                that.addClass('active')



                if (prevSlideType.find('.js_home_type__item__slider').hasClass('slick-initialized')) {
                    prevSlideType.find('.js_home_type__item__slider').slick('unslick')
                }
            });
        }
    })


    $('.js_content_page__rate__tab a').click(function (e) {
        e.preventDefault()
        if (!$(this).hasClass('active')) {
            $('.js_content_page__rate__tab a.active').removeClass('active')
            $(this).addClass('active')
            $('.js_content_page__rate__list__content .content_page__rate__tab__content__item.active').slideUp(400, function () {
                $(this).removeClass('active')
            });
            $('.js_content_page__rate__list__content .content_page__rate__tab__content__item').eq($(this).index()).slideDown(400, function () {
                $(this).addClass('active')
            });
        }
    })


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu__overlay').toggleClass('active')
    });




});